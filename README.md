# vcfdiff

![](https://img.shields.io/badge/written%20in-PHP-blue)

A command-line VCF parser and contact-aware diff utility.

## Changelog

2017-12-21 0.1
- Development
- [⬇️ vcfdiff-0.1.tar.gz](dist-archive/vcfdiff-0.1.tar.gz) *(1.52 KiB)*

